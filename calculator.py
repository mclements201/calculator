

def multiply(value_one, value_two):
    """ Multiple the two provided value and return the product
    
        value_one (double): the first provided value 
        value_two (double): the second provdied value

        return: the product of the two provided values

    """
    return value_one * value_two

def divide(value_one, value_two):
    """ Divide the two provided value and return the quotient
    
        value_one (double): the first provided value
        value_two (double): the second provdied value

        return: the quotient of the two provided values

    """
    return value_one / value_two

def addition(value_one, value_two):
    """ ADD the two provided value and return the sum
    
        value_one (double): the first provided value
        value_two (double): the second provdied value

        return: the sum of the two provided values

    """
    return value_one + value_two

def subtract(value_one, value_two):
    """ Subtract the two provided value and return the Difference
    
        value_one (double): the first provided value
        value_two (double): the second provdied value

        return: the product of the two provided values

    """
    return value_one - value_two


def main():

    # set the default values to the pieces of the equation
    result = 0 # the returned value
    value_one = 0
    value_two = 0
    operator = " "
    
    # ask the user to provide the operation
    maths = True
    while maths:
        n = str(input("Do Maths with -> "))
    
        #    put the user input into a list to find each part of the equation

        n = n.split(" ") 


            # assign the value and operator sign 
        value_one = int(n[0])
        operator = n[1]
        value_two = int(n[2])
            
            

        if operator == "*":
            result = multiply(value_one, value_two)
        elif operator == "/" or operator == "%":
            result = divide(value_one, operator, value_two)
        elif operator == "+":
            result = addition(value_one, value_two)
        elif operator == "-":
            result = subtract(value_one, value_two)
        else:
            maths = False
            
        print(result) 



    # check the user input 


if __name__ == "__main__":
    main()
